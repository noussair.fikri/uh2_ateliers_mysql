package com.uh2.ateliers.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uh2.ateliers.domain.AuthBean;
import com.uh2.ateliers.domain.Client;
import com.uh2.ateliers.service.ClientService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class AuthController {

	
	@GetMapping("auth")
	public AuthBean authentication() {
		return new AuthBean("OK");
	}
	
	
}
