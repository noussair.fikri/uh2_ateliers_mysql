package com.uh2.ateliers.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="UH2_Role")
public class Role {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String nomRole;

	public Role() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Role(String nomRole) {
		super();
		this.nomRole = nomRole;
	}

	public Role(Long id, String nomRole) {
		super();
		this.id = id;
		this.nomRole = nomRole;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomRole() {
		return nomRole;
	}

	public void setNomRole(String nomRole) {
		this.nomRole = nomRole;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", nomRole=" + nomRole + "]";
	}
	
}
