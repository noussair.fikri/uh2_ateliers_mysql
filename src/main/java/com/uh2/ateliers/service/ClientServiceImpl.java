package com.uh2.ateliers.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uh2.ateliers.domain.Client;
import com.uh2.ateliers.dto.ClientDto;
import com.uh2.ateliers.repository.ClientRepository;

@Service
public class ClientServiceImpl implements ClientService {

	@Autowired
	ClientRepository clientRepository;
	
	@Override
	public Client save(ClientDto clientDto) {

		Client clientTmp = new Client();
		clientTmp.setNomClient(clientDto.getNomClient());
		clientTmp.setTelephone(clientDto.getTelephone());
		clientTmp.setEmail(clientDto.getEmail());
		clientTmp.setAdresse(clientDto.getAdresse());
		clientTmp.setFactures(clientDto.getFactures());
		
		clientRepository.save(clientTmp);
		
		return clientTmp;
	}

	@Override
	public Client update(ClientDto clientDto) {
		
		Client clientTmp = new Client();
		clientTmp.setId(clientDto.getId());
		clientTmp.setNomClient(clientDto.getNomClient());
		clientTmp.setTelephone(clientDto.getTelephone());
		clientTmp.setEmail(clientDto.getEmail());
		clientTmp.setAdresse(clientDto.getAdresse());
		clientTmp.setFactures(clientDto.getFactures());
		
		clientRepository.save(clientTmp);
		
		return clientTmp;
	}

	@Override
	public List<Client> findAll() {
		return clientRepository.findAll();
	}

	@Override
	public void remove(ClientDto clientDto) {
		if(clientRepository.findById(clientDto.getId()) != null) {
			clientRepository.deleteById(clientDto.getId());
		}
	}

	@Override
	public Client findByNomClient(String nomClient) {
		return clientRepository.findByNomClient(nomClient);
	}

	@Override
	public Optional<Client> findById(long id) {
		// TODO Auto-generated method stub
		return clientRepository.findById(id);
	}
	

}
