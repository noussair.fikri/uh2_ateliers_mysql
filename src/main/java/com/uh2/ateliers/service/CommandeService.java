package com.uh2.ateliers.service;

import java.util.List;

import com.uh2.ateliers.domain.Commande;
import com.uh2.ateliers.domain.Fournisseur;
import com.uh2.ateliers.dto.CommandeDto;

public interface CommandeService {
	
	Commande save(CommandeDto CommandeDto);
	
	Commande update(CommandeDto CommandeDto);
	
	List<Commande> findAll();
	
	void remove(CommandeDto CommandeDto);
	
	Commande findByReference(String reference);

	List<Commande> findByFournisseur(Fournisseur fournisseur);
}
