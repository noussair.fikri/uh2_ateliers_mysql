package com.uh2.ateliers.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uh2.ateliers.domain.Commande;
import com.uh2.ateliers.domain.Fournisseur;
import com.uh2.ateliers.dto.CommandeDto;
import com.uh2.ateliers.repository.CommandeRepository;

@Service
public class CommandeServiceImpl implements CommandeService {

	
	@Autowired
	CommandeRepository commandeRepository;
	
	
	@Override
	public Commande save(CommandeDto commandeDto) {
		
		Commande commandeTmp = new Commande();
		
		commandeTmp.setFournisseur(commandeDto.getFournisseur());
		commandeTmp.setReference(commandeDto.getReference());
		commandeTmp.setDateExpiration(commandeDto.getDateExpiration());
		commandeTmp.setDesignation(commandeDto.getDesignation());
		commandeTmp.setQuantite(commandeDto.getQuantite());
		commandeRepository.save(commandeTmp);
		
		return commandeTmp;
		
	}

	@Override
	public Commande update(CommandeDto commandeDto) {
		
		Commande commandeTmp = new Commande();
		commandeTmp.setId(commandeDto.getId());
		commandeTmp.setFournisseur(commandeDto.getFournisseur());
		commandeTmp.setReference(commandeDto.getReference());
		commandeTmp.setDateExpiration(commandeDto.getDateExpiration());
		commandeTmp.setDesignation(commandeDto.getDesignation());
		commandeTmp.setQuantite(commandeDto.getQuantite());
		
		commandeRepository.save(commandeTmp);
		
		return commandeTmp;
		
	}

	@Override
	public List<Commande> findAll() {
		return commandeRepository.findAll();
	}

	@Override
	public void remove(CommandeDto commandeDto) {
		if(commandeRepository.findById(commandeDto.getId()) != null) {
			commandeRepository.deleteById(commandeDto.getId());
		}
	}

	@Override
	public Commande findByReference(String reference) {
		return commandeRepository.findByReference(reference);
	}

	@Override
	public List<Commande> findByFournisseur(Fournisseur fournisseur) {
		return findByFournisseur(fournisseur);
	}

}
