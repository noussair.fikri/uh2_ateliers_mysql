package com.uh2.ateliers.service;

import java.util.List;

import com.uh2.ateliers.domain.Facture;
import com.uh2.ateliers.dto.FactureDto;

public interface FactureService {
	
	Facture save(FactureDto factureDto);
	
	Facture update(FactureDto factureDto);
	
	List<Facture> findAll();
	
	void remove(FactureDto factureDto);
	
	Facture findByReference(String reference);

}
