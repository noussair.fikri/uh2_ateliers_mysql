package com.uh2.ateliers.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uh2.ateliers.domain.Facture;
import com.uh2.ateliers.dto.FactureDto;
import com.uh2.ateliers.repository.FactureRepository;

@Service
public class FactureServiceImpl implements FactureService {

	
	@Autowired
	FactureRepository factureRepository;
	
	
	@Override
	public Facture save(FactureDto factureDto) {
		
		Facture factureTmp = new Facture();
		factureTmp.setClient(factureDto.getClient());
		factureTmp.setReference(factureDto.getReference());
		factureTmp.setDateEcheance(factureDto.getDateEcheance());
		factureTmp.setMontantTotal(factureDto.getMontantTotal());
		
		factureRepository.save(factureTmp);
		
		return factureTmp;
		
	}

	@Override
	public Facture update(FactureDto factureDto) {
		
		Facture factureTmp = new Facture();
		factureTmp.setId(factureDto.getId());
		factureTmp.setClient(factureDto.getClient());
		factureTmp.setReference(factureDto.getReference());
		factureTmp.setDateEcheance(factureDto.getDateEcheance());
		factureTmp.setMontantTotal(factureDto.getMontantTotal());
		
		factureRepository.save(factureTmp);
		
		return factureTmp;
		
	}

	@Override
	public List<Facture> findAll() {
		// TODO Auto-generated method stub
		return factureRepository.findAll();
	}

	@Override
	public void remove(FactureDto factureDto) {
		// TODO Auto-generated method stub
		if(factureRepository.findById(factureDto.getId()) != null) {
			factureRepository.deleteById(factureDto.getId());
		}
	}

	@Override
	public Facture findByReference(String reference) {
		// TODO Auto-generated method stub
		return factureRepository.findByReference(reference);
	}

}
