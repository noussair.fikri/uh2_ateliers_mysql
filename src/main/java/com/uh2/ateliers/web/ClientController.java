package com.uh2.ateliers.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uh2.ateliers.domain.Client;
import com.uh2.ateliers.domain.User;
import com.uh2.ateliers.dto.ClientDto;
import com.uh2.ateliers.dto.UserDto;
import com.uh2.ateliers.service.ClientService;
import com.uh2.ateliers.service.UserService;

@Controller
public class ClientController {
	
	@Autowired
	private ClientService clientService;
	
	@GetMapping("/client")
	public String client(Model model)  {
		ClientDto newClient = new ClientDto();
		model.addAttribute("new_client", newClient);
		model.addAttribute("listClients",clientService.findAll());
		return "client";
	}
	
	@PostMapping("/client/add")
	public String client_new(Model model,@ModelAttribute("new_client") @Valid ClientDto clientDto, BindingResult result)  {
		
		Client client = clientService.findByNomClient(clientDto.getNomClient());
		if(client != null) {
			result.rejectValue("nomClient", null, "Ce client existe déjà sur la base");
			model.addAttribute("listClients",clientService.findAll());
		}
		
		if(result.hasErrors()) {
			return "client";
		}
		
		clientService.save(clientDto);
		return "redirect:/client/?success";
		
	}
	
}
